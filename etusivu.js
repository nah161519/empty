const targetNumber = Math.floor(Math.random() * 100) + 1;
let attempts = 0;
const maxAttempts = 5;
function checkGuess() {
    const userGuess = parseInt(document.getElementById('userGuess').value);
    if (isNaN(userGuess) || userGuess < 1 || userGuess > 100) {
        setMessage('Anna kelvollinen numero väliltä 1–100.');
        return;
    }
    attempts++;
    if (userGuess === targetNumber) {
        setMessage(`
        Onnittelut! Arvasit oikean luvun ${targetNumber} ${attempts} yrityksissä.`);
        disableInput();
    } else {
        if (attempts === maxAttempts) {
            setMessage(`Peli ohi! Oikea numero oli ${targetNumber}.`);
            disableInput();
        } else {
            const hint = userGuess < targetNumber ? "liian matala": "liian korkea";
            setMessage(`Väärä arvaus. Yritä uudelleen. Sinun arvauksesi on ${hint}. Yrityksiä jäljellä: ${maxAttempts - attempts}`);
        }
    }
}
function setMessage(message) {
    document.getElementById('message').textContent = message;
}
function disableInput() {
    document.getElementById('userGuess').disabled = true;
    document.querySelector('button').disabled = true;
}